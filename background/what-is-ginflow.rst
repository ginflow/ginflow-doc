What is GinFlow ?
=================

GinFlow is a decentralised workflow engine. As illustrated on the image below,
the GinFlow workflow manager decentralizes the execution coordination of
workflow-based applications by relying on an architecture where multiple
workflow co-engines, *a.k.a.*, service agents (SAs) coordinate each others
through the use of a shared space containing the description of the current
status of the workflow execution.

.. image:: ../images/ginflow.png
   :align: center
   :width: 40%

At run time, the shared space gets fed with the description of the initial
workflow description, namely, its tasks and the dependencies between them.
Also, a set of agents, typically one per task of the workflow get started.
These agents, concurrently, will pull a subportion of the space, more precisely
the information of their assigned task, process it locally, and trigger the
execution of their task if all its incoming dependencies are satisfied. Once
the task is terminated, its results are collected by the agent and forwarded to
their destination agents in a P2P fashion. The shared space is also rewritten
by the agent so as to reflect this termination (and change in the state of the
workflow).

The description of the workflow and its interpretation by the agents rely on an
adaptive, chemistry-inspired rule-based programming language, namely the
*Higher Order Chemical Language* (HOCL). This language relies on concurrent
rewriting of a (multiset) space. You can find more information on HOCL here:
[HOCL]_.

.. [HOCL] Jean-Pierre Banâtre, Pascal Fradet, Yann Radenac: Generalised
   multisets for chemical programming. Mathematical Structures in Computer
   Science 16(4): 557-580 (2006)

Executors
^^^^^^^^^

GinFlow exposes a high-level API (either JSON-based or Java code-based) to
describe your workflow and lets you run it over various distributed
environments. GinFlow supports the following workflow executors:

The *centralised executor* runs your workflow using a single agent
containing an HOCL interpreter responsible for the whole coordination
and invocation of commands realising the tasks.

The *SSH executor* deploys your workflow over a set of agents and fills
the initial shared space. It relies on SSH to connect and start the agents
on a set of worker nodes to be specified in the GinFlow YAML configuration
file.

The *Mesos executor* submits your workflow to a dedicated Mesos\ [#mesos]_
instance managing your worker nodes and uses Mesos primitives to control
the agents.

.. [#mesos] http://mesos.apache.org/

Message brokers
^^^^^^^^^^^^^^^

The communication between agents and between an agent and the shared space
relies on a persistent communication middleware. In practice, two message
brokers are supported: AciveMQ\ [#activemq]_ (which is the default broker)
and the Kafka\ [#kafka]_ / Zookeeper\ [#zookeeper]_ broker (which provides
state recovery of an agent in case it fails). Your preferred broker can be
chosen at set-up, in the YAML configuration file.

.. [#activemq] http://activemq.apache.org/
.. [#kafka] http://kafka.apache.org/
.. [#zookeeper] https://zookeeper.apache.org/


