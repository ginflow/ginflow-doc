Deploying on Grid'5000
=======================


.. On the production nodes (SSH executor only)
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following steps will guide you through the basic use of GinFlow on the
Grid'5000 platform\ [#G5K]_.

1. Install one of the latest ActiveMQ releases (http://activemq.apache.org/download-archives.html)

2. Make a reservation :

::

  screen oarsub -t allow_classic_ssh -l "nodes=N,walltime=XX:YY:ZZ" -I

3. Start ActiveMQ

4. Get the list of machines you reserved:

::

    s=""; for i in $(uniq $OAR_FILE_NODES); do s=$s,$i; done; echo $s

5. In the config file ```ginflow.yaml```
     * copy/paste the list of nodes in the ```ssh.machines``` field.
     * set the other parameters accordingly to your installation.

You are now ready to use the ```ssh executor``` with ActiveMQ.

Note : Depending on your needs Kafka and Zookeeper may replace ActiveMQ.
You can refer to their respective documentations to install them.

.. [#G5K] http://www.grid5000.fr

.. Deploying a Mesos Cluster
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^
