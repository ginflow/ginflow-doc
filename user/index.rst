User Guide
###########

.. toctree::
   :maxdepth: 2

   command-line-interface
   configuration-file
   json-file
   adaptation-mechanisms
   java-api
   deploying-on-grid-5000



