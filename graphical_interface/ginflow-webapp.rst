
Ginflow Webapp
===============

.. image:: ../images/ginflow-webapp.png
   :align: center
   :width: 100%

Ginflow introduces a new way to create a workflow, another way than by command line.
You can now specify a workflow with the graphical user interface, Ginflow Webapp.

The code of this graphical interface is available on BitBucket :
https://bitbucket.org/ginflow/ginflow-webapp

Ginflow Webapp relies on a graph-oriented JavaScript library : Cytoscape.js
The application is an Angular 2 application and communicate with the API Java of Ginflow.

This client application only works when the listen command is launched on Ginflow.
See listen-reference-label_.

With this interface, you can:

- Create a workflow from scratch
- Import workflow by giving a JSON object
- Export the current workflow in a JSON format
- Execute the workflow
- Visualize the state of a service before, during and after the execution

Create your workflow from scratch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Features
##########

- Create an empty service

To create a service, you just have to click on the grey part of the application.

- Update a service

When you create a service, this service is empty. To complete the description of a service, you just have to click on this service, a form will appear on the left of the screen. You can modify its description easily with this.

- Remove a service

To remove a service from the workflow, just right-click on the service.
This will also remove all its connected links.

- Add a src or dst dependency on a service

To do this, you just have to create a link between two services.
For example if you want to create a link between a service "1" and a service "2", "1" will have "2" in its dst and "2" will have "1" in its src.

- Add a src_control or dst_control dependency on a service

It's the same thing before, but when you select the link you can change the dependency data by control.

Import a workflow by giving a JSON
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To import a workflow, you can click on a button "Toggle to import JSON".
Then a text-field will appear on the left, you just have to put a JSON text in it and submit it, then it will create the workflow.

Format of the JSON object
##########################

::

  {
    "name": "wf",
    "services": [
        {
            "position": {
                "x": 279,
                "y": 52
            },
            "service": {
                "description": "Service with id1",
                "dst": [
                    "2",
                    "4"
                ],
                "dstControl": [],
                "id": "1",
                "in": "1",
                "name": "1",
                "src": [],
                "srcControl": [],
                "srv": "echo"
            }
        },
        {
            "position": {
                "x": 274,
                "y": 195
            },
            "service": {
                "description": "Service with id2",
                "dst": [
                    "3"
                ],
                "dstControl": [],
                "id": "2",
                "in": [],
                "name": "2",
                "src": [
                    "1"
                ],
                "srcControl": [],
                "srv": "fdgfgdfgs",
                "sup": "1"
            }
        },
        {
            "position": {
                "x": 275,
                "y": 324
            },
            "service": {
                "description": "Service with id3",
                "dst": [],
                "dstControl": [],
                "id": "3",
                "in": "3",
                "name": "3",
                "src": [
                    "4",
                    "2"
                ],
                "srcControl": [],
                "srv": "echo"
            }
        },
        {
            "position": {
                "x": 525,
                "y": 198
            },
            "service": {
                "alt": "1",
                "description": "Service with id4",
                "dst": [
                    "3"
                ],
                "dstControl": [],
                "id": "4",
                "in": "4",
                "name": "4",
                "src": [
                    "1"
                ],
                "srcControl": [],
                "srv": "echo"
            }
        }
    ]
  }

The description looks like the workflow description that ginflow takes to execute the workflow, but we must define an object position to know where the service will be created on the screen.


Get the JSON object of the workflow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A button "Export to JSON" allows you to get the JSON text of the current workflow.
The JSON format is the same as the format to import workflow.

Execute the workflow
^^^^^^^^^^^^^^^^^^^^^

Prerequisites
#############

Ginflow must be launched with the listen command before launching the workflow.

The button "Launch" will launch the workflow, just like the launch command of Ginflow.

Visualize the result of a service
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When the execution of a service is terminated, you can select it to show different informations like:

- The exit code
- The standard output
- The error output

A service which has failed, will be colored in red.
A service which has been executed successfully will be colored in green.