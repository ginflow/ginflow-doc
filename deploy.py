import conf


TARGET = "hocl@hocl-server"
DIRECTORY = "/var/www/downloads/documentation/"

KEY = "/Users/msimonin/.ssh/hocl-server/id_rsa_hocl-server"


print("Deploying documentation for {} to {}:{}".format(conf.release, TARGET, DIRECTORY))

print("ssh -i {} {} -C 'mkdir -p {}'".format(KEY, TARGET, DIRECTORY + conf.release))
print("rsync -avz -e 'ssh -i {}' _build/* {}:{}".format(KEY, TARGET, DIRECTORY + conf.release))

