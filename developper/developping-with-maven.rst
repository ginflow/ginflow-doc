Developping with Maven
=======================

Set up your project
^^^^^^^^^^^^^^^^^^^

You can add the GinFlow artifact as a maven dependency to your project.
This can be done by adding the following lines to your *pom.xml*

::

  <repository>
    <id>ginflow-snapshots-repository</id>
    <name>ginflow-snapshots-repository</name>
    <url>http://hocl-server.irisa.fr/downloads/maven/snapshots</url>
  </repository>
  <repository>
    <id>ginflow-releases-repository</id>
    <name>ginflow-releases-repository</name>
    <url>http://hocl-server.irisa.fr/downloads/maven/releases</url>
  </repository>

  <dependency>
    <groupId>fr.inria</groupId>
    <artifactId>hocl-workflow</artifactId>
    <version>X.Y.Z</version>
  </dependency>

Fill the hocl-workflow version accordingly to your needs.


Using the Java API
^^^^^^^^^^^^^^^^^^

This examples are extracted from the test suite. They aim at illustrating
how to build and launch a workflow programmatically.

First we define a method to build the i-th service in a numbered sequence
of services. Note that the first and the last service in a sequence need
a special attention regarding their sources and destinations:

::

     /**
      *
      * Construct a service in sequence.
      *
      * Ti-1 -> Ti -> Ti+1
      *
      * @param max       max number of service in the sequence
      * @param i         current id in the sequence
      * @param command   the command to run
      * @return  The built service.
      */
     protected Service Ti(int max, int i, String command) {
         Service service = Service.newService(String.valueOf(i))
                                 .setCommand(Command.newCommand(command));
         Destination destination = Destination.newDestination();
         Source source = Source.newSource();
         if (i < max - 1) {
             destination = Destination.newDestination()
                     .add(String.valueOf(i + 1));
         }
         if (i>0) {
             source = Source.newSource()
                     .add(String.valueOf(i - 1));
         }

         return service.setSource(source)
                 .setDestination(destination);
     }

Next we define the workflow containing all the services in sequences (in
this case, all services are executing the *echo* command):

::

    /**
     *
     * Build a sequence of max services, each running the same command.
     *
     * @param max           The number of services
     * @param command       The command
     * @return
     */
    protected Workflow sequence(int max, String command) {
        Workflow workflow = Workflow.newWorkflow("sequence");
        for (int i = 0 ; i < max ; i++) {
            workflow.registerService(String.valueOf(i), Ti(max, i, command));
        }
        // set the last service has terminal.
        workflow.getTerminalServices().add(String.valueOf(max - 1));
        return workflow;
    }

Then we can instantiate a workflow and an executor object to handle the
execution of the workflow:


::

    int max = 2;
    Workflow workflow = sequence(max, "echo !");
    GinflowExecutor executor = new CentralisedExecutor(workflow, new Options());
    executor.decorate();
    executor.execute();


To go further, please refer to the API documentation.
